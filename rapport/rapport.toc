\babel@toc {french}{}
\contentsline {chapter}{\numberline {1}Notice utilisateur}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}pr\IeC {\'e}sentation}{3}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Structure du code }{4}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Initialisation du jeu}{4}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Cr\IeC {\'e}ation des \IeC {\'e}l\IeC {\'e}ments de jeu}{4}{section.2.2}% 
\contentsline {section}{\numberline {2.3}D\IeC {\'e}placement du joueur}{4}{section.2.3}% 
\contentsline {section}{\numberline {2.4}G\IeC {\'e}n\IeC {\'e}rer des ennemis}{5}{section.2.4}% 
\contentsline {section}{\numberline {2.5}D\IeC {\'e}placer les ennemis et collision avec le joueur}{6}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Cr\IeC {\'e}ation et d\IeC {\'e}placement des tirs}{7}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Gestion missiles}{8}{section.2.7}% 
\contentsline {section}{\numberline {2.8}Tuer les ennemis}{9}{section.2.8}% 
\contentsline {section}{\numberline {2.9}Gestion du jeu}{9}{section.2.9}% 
\contentsline {section}{\numberline {2.10}GameOver}{9}{section.2.10}% 
\contentsline {chapter}{\numberline {3}Choix de d\IeC {\'e}veloppement}{10}{chapter.3}% 
\contentsline {chapter}{\numberline {4}R\IeC {\'e}f\IeC {\'e}rence}{11}{chapter.4}% 
